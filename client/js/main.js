
	import "babel-polyfill";
	let ie9 = document.querySelector('body').classList.contains('ie9');
	let listData;


	let standardData = [{

			"Name": "hotelone",
			"image": "http://static.laterooms.com/hotelphotos/laterooms/286755/gallery/286755.jpg",
			"StarRating": 5,
			"Facilities": ["car park", "pool"]

		},

		{

			"Name": "hoteltwo",
			"image": "http://static.laterooms.com/hotelphotos/laterooms/286755/gallery/286755.jpg",
			"StarRating": 3,
			"Facilities": ["car park", "gym"]

		},

		{

			"Name": "hotelthree",
			"image": "http://static.laterooms.com/hotelphotos/laterooms/286755/gallery/286755.jpg",
			"StarRating": 3,
			"Facilities": []

		}
	];


	function generateList(data) {
		let lister = document.querySelector('.lister');

		if(data) {
			listData = data;
		} else {
			listData = standardData;
		}

		lister.innerHTML = '';
		listData.map(hotel => {

			let features = hotel.Facilities.map(feature => {
				return `<li>${feature}</li>`
			});
			let goldRating = '';
			let noFeatures = '';
			if(hotel.StarRating === 5){
				goldRating = 'goldRating'
			}

			if(features.length === 0) {
				noFeatures = 'hide';
			}

			lister.innerHTML += `<li class="hotelItem" ><img src="${hotel.image}" /><h2>${hotel.Name}</h2> <p>Star Rating</p><p class="rating"><span class="${goldRating}">${hotel.StarRating}</span> / 5</p> <p class="facilities ${noFeatures}">Facilities</p><ul>${features}</ul></li>`;

		});
	}

	function searchSetup() {

		let facilities = [];
		standardData.map(hotel => {
			hotel.Facilities.map(item => {
				if(facilities.indexOf(item) === -1) {
					facilities.push(item);
				}
			});
		});

		facilities.map(item => {
			let test = `<div class="input"><input type="radio" class="radioBtn" name="features" value="${item}" /> ${item}</div>`;
			document.getElementById('hotelSearch').innerHTML += test;
		});

	}



	function filterList(term) {
		document.querySelector('.lister').innerHTML = '';


		if(term === 'all') {
			listData = standardData;
		} else {

			listData = standardData.filter(el => {
				return (el.Facilities.indexOf(term) > -1);
			});

		}

			generateList(listData);






	}

	function compareLow(a, b) {
		console.log(a,b)
	  if (a.StarRating > b.StarRating) {
	    return 1;
	  }
	  if (a.StarRating < b.StarRating) {
	    return -1;
	  }
	  // a must be equal to b
	  return 0;
	}

	function compareHigh(a, b) {
	  if (a.StarRating < b.StarRating) {
	    return 1;
	  }
	  if (a.StarRating > b.StarRating) {
	    return -1;
	  }
	  // a must be equal to b
	  return 0;
	}

	function sortList(value) {

		switch(value) {
			case 'low':

				listData.sort(function(a, b) {
    			return parseFloat(a.StarRating) - parseFloat(b.StarRating);
				});

				generateList(listData);
			break;
			case 'high':
				listData.sort(function(a, b) {
    			return parseFloat(b.StarRating) - parseFloat(a.StarRating);
				});

					generateList(listData);
			break;
			default :

				generateList(listData);
		}
	}



	document.addEventListener('DOMContentLoaded', () => {
		generateList();
		searchSetup();

			let btns = document.querySelectorAll('.radioBtn');
			[...btns].map(btn => {
				btn.addEventListener('change', e => {
					filterList(btn.value);
				});
			});

			let sortBtn = document.getElementById('ratingSort');
			sortBtn.addEventListener('change', e => {
				console.log('hi')
				sortList(sortBtn.value);
			});

	});
